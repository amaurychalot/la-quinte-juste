import React, { useState } from "react";
import './App.scss';
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import Info from "./components/Info/Info";
//import Scene from "./components/Scene/Scene";

function App() {
  const [index, setIndex] = useState(-1);
  const [isInfo, setIsInfo] = useState(false);

  return (
    <div class="app">
      <Header />
      {isInfo ? (<Info index={index} setIndex={setIndex} setIsInfo={setIsInfo} />) : (<Home setIndex={setIndex} setIsInfo={setIsInfo} />)}

    </div>
  );
}

export default App;
