import React, { Component } from 'react';
import * as THREE from 'three';
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader';
import { HDRCubeTextureLoader } from 'three/examples/jsm/loaders/HDRCubeTextureLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { BokehPass } from 'three/examples/jsm/postprocessing/BokehPass';
import { Color, Raycaster } from 'three';
import { GUI } from 'dat.gui';

class SceneInfo extends Component {
    constructor(props) {
        super(props)

        this.index = this.props.index;
        this.paths = [
            "/models/violon/violon.gltf",
            "/models/sax/scene.gltf",
            "/models/guitare/scene.gltf",
            "/models/batterie/scene.gltf",
            "/models/piano/scene.gltf",
            "/models/balafon/scene.gltf",
            "/models/electric_guitar/scene.gltf",
            "/models/sanza/scene.gltf",
            "/models/violoncelle/scene.gltf",
            "/models/flute/scene.gltf",
        ]

        this.stateItem = [{
            selected: false,
            position: {
                x: 0,
                y: -40,
                z: 0,
            },
            zoomPosition: {
                x: 0,
                y: -70,
                z: 0,
            },
            size: 1,
            zoomSize: 1.5,
        },
        {
            selected: false,
            position: {
                x: 0,
                y: 0,
                z: 0,
            },
            zoomPosition: {
                x: 0,
                y: -4,
                z: 0,
            },
            size: 0.22,
            zoomSize: 0.29,
        },
        {
            selected: false,
            position: {
                x: 0,
                y: -60,
                z: 0,
            },
            zoomPosition: {
                x: 0,
                y: -70,
                z: 0,
            },
            size: 100,
            zoomSize: 130,
        },
        {
            selected: false,
            position: {
                x: 0,
                y: -40,
                z: -15,
            },
            zoomPosition: {
                x: 0,
                y: -40,
                z: -15,
            },
            size: 0.04,
            zoomSize: 0.04,
        },
        {
            selected: false,
            position: {
                x: 0,
                y: -35,
                z: 0,
            },
            zoomPosition: {
                x: 0,
                y: -35,
                z: 0,
            },
            size: 0.5,
            zoomSize: 0.5,
        },
        {
            selected: false,
            position: {
                x: 0,
                y: 0,
                z: 0,
            },
            zoomPosition: {
                x: 0,
                y: 0,
                z: 0,
            },
            size: 2,
            zoomSize: 2.6,
        },
        {
            selected: false,
            position: {
                x: 0,
                y: 0,
                z: 0,
            },
            zoomPosition: {
                x: 0,
                y: 0,
                z: 0,
            },
            size: 0.8,
            zoomSize: 0.8,
        },
        {
            selected: false,
            position: {
                x: 0,
                y: -50,
                z: 0,
            },
            zoomPosition: {
                x: 0,
                y: -50,
                z: 0,
            },
            size: 2.7,
            zoomSize: 3.1,
        },
        {
            selected: false,
            position: {
                x: 0,
                y: -10,
                z: 0,
            },
            zoomPosition: {
                x: 0,
                y: -10,
                z: 0,
            },
            size: 0.08,
            zoomSize: 0.11,
        },
        {
            selected: false,
            position: {
                x: 0,
                y: 0,
                z: 0,
            },
            zoomPosition: {
                x: 0,
                y: 0,
                z: 0,
            },
            size: 8,
            zoomSize: 10,
        }];

        this.palette = {
            ambiantColor: '#ffffff',
            directionalColor: '#ffffff',
            // CSS string
            // color2: [ 0, 128, 255 ], // RGB array
            // color3: [ 0, 128, 255, 0.3 ], // RGB with alpha
            // color4: { h: 350, s: 0.9, v: 0.3 } // Hue, saturation, value
        };

        this.effectOptions = {
            enabledDof: false,
            aperture: 1,
            focus: 100,
            maxblur: 0.01,
        }

        //this.gui = new GUI();
        this.mouse = new THREE.Vector2();
        this.raycaster = new Raycaster();
        this.start = this.start.bind(this);
        this.stop = this.stop.bind(this);
        this.animate = this.animate.bind(this);

        // this.gui.addColor(palette, 'color2');
        // this.gui.addColor(palette, 'color3');
        // this.gui.addColor(palette, 'color4');
    }

    componentDidMount() {
        const width = this.mount.clientWidth;
        const height = this.mount.clientHeight;

        
        this.gui = new GUI();

        const scene = new THREE.Scene();
        const camera = new THREE.PerspectiveCamera(
            75,
            width / height,
            0.1,
            1000
        );

        const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        camera.position.z = 100;
        renderer.setSize(width, height);
        renderer.setClearColor(0xFFFFFF, 0);

        this.scene = scene
        this.camera = camera
        this.renderer = renderer

        //Cube Map
        const pmremGenerator = new THREE.PMREMGenerator(this.renderer);
        pmremGenerator.compileCubemapShader();
        const hdrTexture = new HDRCubeTextureLoader().load(['cubemap/px.hdr', 'cubemap/nx.hdr', 'cubemap/py.hdr', 'cubemap/ny.hdr', 'cubemap/pz.hdr', 'cubemap/nz.hdr']);
        scene.background = hdrTexture;

        //loadItem
        const loader = new GLTFLoader();
        loader.load(
            this.paths[this.index], (gltf) => {
                this.model = gltf.scene;
                if (this.paths[this.index] === "/models/violoncelle/scene.gltf") {
                    this.model.children[0].children[0].position.set(0, 0, 0);
                    this.model.children[0].children[0].rotation.set(0, 0, 0);
                    this.model.children[0].rotation.set(-Math.PI / 2, Math.PI / 32, -Math.PI / 8);
                }
                else if (this.paths[this.index] === "/models/sanza/scene.gltf") {
                    this.model.children[0].position.set(0, 0, 0);
                    this.model.children[0].rotation.z = -Math.PI / 8;
                    this.model.children[0].rotation.y = -Math.PI / 3;
                }
                else if (this.paths[this.index] === "/models/electric_guitar/scene.gltf") {
                    this.model.children[0].rotation.set(0, 0, 0);
                    this.model.children[0].children[0].rotation.set(0, 0, Math.PI / 2);
                }
                else if (this.paths[this.index] === "/models/piano/scene.gltf") {
                    this.model.children[0].position.set(-500, 0, 0);
                }
                else if (this.paths[this.index] === "/models/sax/scene.gltf") {
                    this.model.children[0].children[0].children[0].children[0].children.shift();
                }

                else if (this.paths[this.index] === "/models/guitare/scene.gltf") {
                    this.model.children[0].children[0].position.set(0, 0, 0);
                    this.model.children[0].children[0].rotation.set(0, 0, -Math.PI / 2);
                }

                this.model.position.set(this.stateItem[this.index].position.x, this.stateItem[this.index].position.y, this.stateItem[this.index].position.z);
                this.model.scale.set(this.stateItem[this.index].size, this.stateItem[this.index].size, this.stateItem[this.index].size);
                scene.add(this.model)
            }
        );

        
        //AmbientLight
        this.ambientLight = new THREE.AmbientLight(this.palette.ambiantColor);
        this.ambientLight.intensity = 1;
        
        //DirectionalLight
        this.dirLight = new THREE.DirectionalLight(this.palette.directionalColor);
        this.dirLight.intensity = 5;
        this.dirLight.position.set(0, 100, 100);
        this.dirLight.target.position.set(0, 0, 0);
        
        this.gui.addColor(this.palette, 'ambiantColor').onChange((value) => {
            //debugger
            scene.remove(this.ambientLight);
            this.ambientLight = new THREE.AmbientLight(value);
            this.ambientLight.intensity = 1;
            scene.add(this.ambientLight);
        });
        
        this.gui.addColor(this.palette, 'directionalColor').onChange((value) => {
            //debugger
            scene.remove(this.dirLight);
            this.dirLight = new THREE.DirectionalLight(value);
            this.dirLight.position.set(0, 100, 100);
            this.dirLight.target.position.set(0, 0, 0);
            this.dirLight.intensity = 5;
            scene.add(this.dirLight);
        });

        scene.add(this.dirLight);
        scene.add(this.ambientLight);


        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.dampingFactor = 0.05;
        this.controls.enableDamping = true;
        this.controls.target.set(0, 3.5, -3);
        this.controls.update();

        this.mount.appendChild(this.renderer.domElement);

        //PostProcessing
        this.composer = new EffectComposer(this.renderer);
        const renderPass = new RenderPass(this.scene, this.camera);
        this.composer.addPass(renderPass);



        this.dof = new BokehPass(this.scene, this.camera, {
            aperture: this.effectOptions.aperture * 0.0001,
            focus: this.effectOptions.focus,
            maxblur: this.effectOptions.maxblur,
        });

        //this.dof.renderToScreen = true;
        this.dof.renderTargetDepth.setSize(width, height);
        this.composer.addPass(this.dof);
        this.dof.enabled = this.effectOptions.enabledDof;
        console.log(this.dof);

        this.gui.add(this.effectOptions, 'enabledDof').onChange((value) => {
            //debugger
            this.dof.enabled = value;
            this.composer.addPass(this.dof);

        });

        this.gui.add(this.effectOptions, "focus", 0.0, 200.0, 5).onChange((value) => {
            this.composer.removePass(this.dof);
            this.dof = new BokehPass(this.scene, this.camera, {
                aperture: this.effectOptions.aperture  * 0.0001,
                focus: value,
                maxblur: this.effectOptions.maxblur,
            });
            this.dof.renderTargetDepth.setSize(width, height);
            this.dof.enabled = this.effectOptions.enabledDof;
            this.composer.addPass(this.dof);
        });

        this.gui.add(this.effectOptions, "aperture", 0, 20, 0.1).onChange((value) => {
            this.composer.removePass(this.dof);
            this.dof = new BokehPass(this.scene, this.camera, {
                aperture: value * 0.0001,
                focus: this.effectOptions.focus,
                maxblur: this.effectOptions.maxblur,
            });
            this.dof.renderTargetDepth.setSize(width, height);
            this.dof.enabled = this.effectOptions.enabledDof;
            this.composer.addPass(this.dof);
        });

        this.gui.add(this.effectOptions, "maxblur", 0.0, 0.02, 0.001).onChange((value) => {
            this.composer.removePass(this.dof);
            this.dof = new BokehPass(this.scene, this.camera, {
                aperture: this.effectOptions.aperture  * 0.0001,
                focus: this.effectOptions.focus,
                maxblur: value,
            });
            this.dof.renderTargetDepth.setSize(width, height);
            this.dof.enabled = this.effectOptions.enabledDof;
            this.composer.addPass(this.dof);
        });

        this.gui.close();
        this.composer.setSize(width, height);


        this.renderer.autoClear = false;

        this.start()
    }

    componentWillUnmount() {
        this.renderer.dispose();
        this.raycaster.dispose();
        this.gui.dispose();
        this.stop();
    }
    
    start() {
        if (!this.frameId) {
          this.frameId = requestAnimationFrame(this.animate);
        }
    }

    stop() {
        cancelAnimationFrame(this.frameId)
    }

    changeItem() {
        this.index = this.props.index;
        this.scene.remove(this.model);
        const loader = new GLTFLoader();
        loader.load(
            this.paths[this.index], (gltf) => {
                this.model = gltf.scene;
                if (this.paths[this.index] === "/models/violoncelle/scene.gltf") {
                    this.model.children[0].children[0].position.set(0, 0, 0);
                    this.model.children[0].children[0].rotation.set(0, 0, 0);
                    this.model.children[0].rotation.set(-Math.PI / 2, Math.PI / 32, -Math.PI / 8);
                }
                else if (this.paths[this.index] === "/models/sanza/scene.gltf") {
                    this.model.children[0].position.set(0, 0, 0);
                    this.model.children[0].rotation.z = -Math.PI / 8;
                    this.model.children[0].rotation.y = -Math.PI / 3;
                }
                else if (this.paths[this.index] === "/models/electric_guitar/scene.gltf") {
                    this.model.children[0].rotation.set(0, 0, 0);
                    this.model.children[0].children[0].rotation.set(0, 0, Math.PI / 2);
                }
                else if (this.paths[this.index] === "/models/piano/scene.gltf") {
                    this.model.children[0].position.set(-500, 0, 0);
                }
                else if (this.paths[this.index] === "/models/sax/scene.gltf") {
                    this.model.children[0].children[0].children[0].children[0].children.shift();
                }

                else if (this.paths[this.index] === "/models/guitare/scene.gltf") {
                    this.model.children[0].children[0].position.set(0, 0, 0);
                    this.model.children[0].children[0].rotation.set(0, 0, -Math.PI / 2);
                }

                this.model.position.set(this.stateItem[this.index].position.x, this.stateItem[this.index].position.y, this.stateItem[this.index].position.z);
                this.model.scale.set(this.stateItem[this.index].size, this.stateItem[this.index].size, this.stateItem[this.index].size);
                this.scene.add(this.model)
            }
        );
    }

    animate() {
        //this.composer.renderer.clear();

        if (this.index !== this.props.index) {
            this.changeItem();
        }

        if (this.dirLight.intensity === 5 && (this.paths[this.index] === "/models/sax/scene.gltf" || this.paths[this.index] === "model/flute/scene.gltf"))
            this.dirLight.intensity = 50;
        else if (this.dirLight.intensity === 50 && this.paths[this.index] !== "/models/sax/scene.gltf" && this.paths[this.index] !== "model/flute/scene.gltf")
            this.dirLight.intensity = 5;

        this.controls.update();
        this.renderScene();
        this.composer.render();
        this.frameId = window.requestAnimationFrame(this.animate);
    }

    renderScene() {
        this.renderer.render(this.scene, this.camera);
    }

    render() {
        return (
          <div
            style={{ width: '45vw', height: "65vh" }}
            ref={(mount) => { this.mount = mount }}
          />
        )
    }
}

export default SceneInfo;