import "./Header.scss";

export default function Header() {
    return (
        <header>
            <div className="header-inner">
                <div className="logo">La Quinte Juste.</div>
                <nav>
                    <ul>
                        <li>
                            <a>Qui sommes nous ?</a>
                        </li>
                        <li>
                            <a href="/">Nos Produits</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
    );
}