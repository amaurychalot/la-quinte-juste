import React, { Component } from 'react';
import * as THREE from 'three';
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { OutlinePass } from 'three/examples/jsm/postprocessing/OutlinePass';
//import { BokehPass } from 'three/examples/jsm/postprocessing/BokehPass';
import { SSAOPass } from 'three/examples/jsm/postprocessing/SSAOPass';
//import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls'

class Scene extends Component {

  constructor(props) {
    super(props)

    this.start = this.start.bind(this);
    this.stop = this.stop.bind(this);
    this.animate = this.animate.bind(this);
  }

  componentDidMount() {
    const width = this.mount.clientWidth;
    const height = this.mount.clientHeight;

    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(
      75,
      width / height,
      0.1,
      1000
    );
    const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    const light = new THREE.AmbientLight(0xffffff);
    light.intensity = 1;
    scene.add(light);
    const dirLight = new THREE.DirectionalLight(0xffffff);
    dirLight.position.set(10, 10, 5);
    dirLight.intensity = 5;
    scene.add(dirLight);
    /*const controls = new OrbitControls(camera, renderer.domElement);
    controls.update();*/



    //Load 3DObject
    const loader = new GLTFLoader();
    loader.load(
      this.props.path, (gltf) => {
        this.model = gltf.scene;
        if (this.props.path === "/models/violoncelle/scene.gltf") {
          this.model.children[0].children[0].position.set(0, 0, 0);
          this.model.children[0].children[0].rotation.set(0, 0, 0);
          this.model.children[0].rotation.set(-Math.PI / 2, Math.PI / 32, -Math.PI / 8);
        }
        else if (this.props.path === "/models/sanza/scene.gltf") {
          this.model.children[0].position.set(0, 0, 0);
          this.model.children[0].rotation.z = -Math.PI / 8;
          this.model.children[0].rotation.y = -Math.PI / 3;
        }
        else if (this.props.path === "/models/electric_guitar/scene.gltf") {
          this.model.children[0].rotation.set(0, 0, 0);
          this.model.children[0].children[0].rotation.set(0, 0, Math.PI / 2);
        }
        else if (this.props.path === "/models/piano/scene.gltf") {
          this.model.children[0].position.set(-500, 0, 0);
        }
        else if (this.props.path === "/models/sax/scene.gltf")
          this.model.children[0].children[0].children[0].children[0].children.shift();
        else if (this.props.path === "/models/guitare/scene.gltf")
        {
          this.model.children[0].children[0].position.set(0, 0, 0);
          this.model.children[0].children[0].rotation.set(0, 0, -Math.PI / 2);
        }
        this.model.position.set(this.props.stateItem.position.x, this.props.stateItem.position.y, this.props.stateItem.position.z);
        this.model.scale.set(this.props.stateItem.size, this.props.stateItem.size, this.props.stateItem.size)
        scene.add(this.model)
      }
    );

    camera.position.z = 120;
    renderer.setClearColor(0x000000, 0);
    renderer.setSize(width, height);

    this.scene = scene
    this.camera = camera
    this.renderer = renderer

    //PostProcessing
    const param = {minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, format: THREE.RGBAFormat, stencilBuffer: false};
    const renderTarget = new THREE.WebGLRenderTarget(width, height, param);
    this.composer = new EffectComposer(this.renderer, renderTarget);
    const renderPass = new RenderPass(this.scene, this.camera);
    this.composer.addPass(renderPass);
    


    this.ssaoPass = new SSAOPass(this.scene, this.camera, width, height);
    this.ssaoPass.kernelRadius = 16;
    this.ssaoPass.output = 0;
    this.ssaoPass.minDistance = 0.005;
    this.ssaoPass.maxDistance = 0.1;
    this.composer.addPass(this.ssaoPass);

    this.outlinePass = new OutlinePass(new THREE.Vector2(window.innerWidth, window.innerHeight), this.scene, this.camera);
    this.composer.addPass(this.outlinePass);

    this.composer.setSize(width, height);
    this.renderer.autoClear = false;

    this.mount.appendChild(this.renderer.domElement)
    this.start()

  }

  componentWillUnmount() {
    this.stop()
    //this.mount.removeChild(this.renderer.domElement)
  }

  start() {
    if (!this.frameId) {
      this.frameId = requestAnimationFrame(this.animate)
    }
  }

  stop() {
    cancelAnimationFrame(this.frameId)
  }

  animate() {
    //this.cube.rotation.x += 0.01
    //this.model.rotation.y += 0.01
    if (this.model && this.props.stateItem.selected)
    {
      if (this.model.scale.x < this.props.stateItem.zoomSize)
      {
        this.model.scale.x += (this.props.stateItem.zoomSize - this.props.stateItem.size) / 12;
        this.model.scale.y += (this.props.stateItem.zoomSize - this.props.stateItem.size) / 12;
        this.model.scale.z += (this.props.stateItem.zoomSize - this.props.stateItem.size) / 12;
        this.model.position.y += (this.props.stateItem.zoomPosition.y - this.props.stateItem.position.y) / 12;
      }
    }

    else if (this.model && !this.props.stateItem.selected)
    {
      if (this.model.scale.x > this.props.stateItem.size)
      {
        this.model.scale.x -= (this.props.stateItem.zoomSize - this.props.stateItem.size) / 12;
        this.model.scale.y -= (this.props.stateItem.zoomSize - this.props.stateItem.size) / 12;
        this.model.scale.z -= (this.props.stateItem.zoomSize - this.props.stateItem.size) / 12;
        this.model.position.y -= (this.props.stateItem.zoomPosition.y - this.props.stateItem.position.y) / 12;
      }
    }

    if (this.model && this.props.stateItem.selected)
    {
      this.model.rotation.y += 0.01;
      const selectedObject = [];
      selectedObject.push(this.model);
      this.outlinePass.selectedObjects = selectedObject;
    }

    else if (this.model && !this.props.stateItem.selected)
    {
      this.model.rotation.y = 0;
      const selectedObject = [];
      this.outlinePass.selectedObjects = selectedObject;
    }
    this.composer.render();
    this.frameId = window.requestAnimationFrame(this.animate);
    this.renderScene();
  }

  renderScene() {
    this.renderer.render(this.scene, this.camera)
  }

  render() {
    return (
      <div
        style={{ width: '400px', height: "400px" }}
        ref={(mount) => { this.mount = mount }}
      />
    )
  }
}

export default Scene;