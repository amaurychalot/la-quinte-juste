import "./Home.scss"
import Scene from "../Scene/Scene";

export default function Home({setIndex, setIsInfo}) {

    const path1 = "/models/violon/violon.gltf";
    const path2 = "/models/sax/scene.gltf";
    const path3 = "/models/guitare/scene.gltf";
    const path4 = "/models/batterie/scene.gltf";
    const path5 = "/models/piano/scene.gltf";
    const path6 = "/models/balafon/scene.gltf";
    const path7 = "/models/electric_guitar/scene.gltf";
    const path8 = "/models/sanza/scene.gltf";
    const path9 = "/models/violoncelle/scene.gltf";
    const path10 = "/models/flute/scene.gltf";

    const stateItem1 = {
        selected: false,
        position: {
            x: 0,
            y: -40,
            z: 0,
        },
        zoomPosition: {
            x: 0,
            y: -70,
            z: 0,
        },
        size: 1,
        zoomSize: 1.5,
    }
    const stateItem2 = {
        selected: false,
        position: {
            x: 0,
            y: 0,
            z: 0,
        },
        zoomPosition: {
            x: 0,
            y: -4,
            z: 0,
        },
        size: 0.22,
        zoomSize: 0.29,
    }
    const stateItem3 = {
        selected: false,
        position: {
            x: 0,
            y: -60,
            z: 0,
        },
        zoomPosition: {
            x: 0,
            y: -70,
            z: 0,
        },
        size: 100,
        zoomSize: 130,
    }
    const stateItem4 = {
        selected: false,
        position: {
            x: 0,
            y: -40,
            z: -15,
        },
        zoomPosition: {
            x: 0,
            y: -40,
            z: -15,
        },
        size: 0.035,
        zoomSize: 0.04,
    }
    const stateItem5 = {
        selected: false,
        position: {
            x: 0,
            y: -35,
            z: 0,
        },
        zoomPosition: {
            x: 0,
            y: -35,
            z: 0,
        },
        size: 0.4,
        zoomSize: 0.5,
    }
    const stateItem6 = {
        selected: false,
        position: {
            x: 0,
            y: 0,
            z: 0,
        },
        zoomPosition: {
            x: 0,
            y: 0,
            z: 0,
        },
        size: 2,
        zoomSize: 2.6,
    }
    const stateItem7 = {
        selected: false,
        position: {
            x: 0,
            y: 0,
            z: 0,
        },
        zoomPosition: {
            x: 0,
            y: 0,
            z: 0,
        },
        size: 0.65,
        zoomSize: 0.8,
    }
    const stateItem8 = {
        selected: false,
        position: {
            x: 0,
            y: -50,
            z: 0,
        },
        zoomPosition: {
            x: 0,
            y: -50,
            z: 0,
        },
        size: 2.7,
        zoomSize: 3.1,
    }
    const stateItem9 = {
        selected: false,
        position: {
            x: 0,
            y: -10,
            z: 0,
        },
        zoomPosition: {
            x: 0,
            y: -10,
            z: 0,
        },
        size: 0.08,
        zoomSize: 0.11,
    }
    const stateItem10 = {
        selected: false,
        position: {
            x: 0,
            y: 0,
            z: 0,
        },
        zoomPosition: {
            x: 0,
            y: 0,
            z: 0,
        },
        size: 8,
        zoomSize: 10,
    }

    return (
        <div className="homeContainer">
            <div className={"itemContainer"} onMouseEnter={() => {
                        stateItem1.selected = true;
                    }}
                    onMouseLeave={() => {
                        stateItem1.selected = false;
                    }}
                    onClick={() => {
                        setIndex(0);
                        setIsInfo(true);
                    }} >
                <div className="instrumentImg">
                    <Scene className="instrumentImg" path={path1} stateItem={stateItem1} />
                </div>
                <div className="instrumentText">
                    <h2 className="title">Violon</h2>
                    <p>
                    Ce magnifique violon massif 4/4 pourra vous accompagner dans toute votre splendeur. Etudié pour une grande durée de vie avec des matériaux choisis avec soin, ce violon a été conçu de la musique de chambre à la musique d'orchestrale.
                    </p>
                </div>
            </div>
            <div className={"itemContainer"} onMouseEnter={() => {
                        stateItem2.selected = true;
                    }}
                    onMouseLeave={() => {
                        stateItem2.selected = false;
                    }}
                    onClick={() => {
                        setIndex(1);
                        setIsInfo(true);
                    }} >
                <div className="instrumentImg">
                    <Scene path={path2} stateItem={stateItem2} />
                </div>
                <div className="instrumentText">
                    <h2 className="title">Saxophone</h2>
                    <p>
                        Un bec conçu pour révéler le meilleur de votre son. Ce saxophone est un parfail mélange entre facilité de jeu et justesse des notes. Il s'agit du parfait instrument pour briller sur scène.
                    </p>
                </div>
            </div>
            <div className={"itemContainer"} onMouseEnter={() => {
                        stateItem3.selected = true;
                    }}
                    onMouseLeave={() => {
                        stateItem3.selected = false;
                    }}
                    onClick={() => {
                        setIndex(2);
                        setIsInfo(true);
                    }} >
                <div className="instrumentImg">
                    <Scene className="instrumentImg" path={path3} stateItem={stateItem3} />
                </div>
                <div className="instrumentText">
                    <h2 className="title">Guitare acoustique</h2>
                    <p>
                    Cette guitare acoustique folk est disponible dès maintenant, idéale pour débuter la guitare, elle a une très bonne prise en main, avec un poids de 2kg, vous pourrez la transporter partout !
                    </p>
                </div>
            </div>
            <div className={"itemContainer"} onMouseEnter={() => {
                        stateItem4.selected = true;
                    }}
                    onMouseLeave={() => {
                        stateItem4.selected = false;
                    }}
                    onClick={() => {
                        setIndex(3);
                        setIsInfo(true);
                    }} >
                <div className="instrumentImg">
                    <Scene className="instrumentImg" path={path4} stateItem={stateItem4} />
                </div>
                <div className="instrumentText">
                    <h2 className="title">Batterie</h2>
                    <p>
                    Avec cette batterie votre jeu et votre créativité ne seront pas seulement parfaitement reproduits, mais également mis en valeur.
                    Fûts fabriqués mains à partir de tilleul, équipée désormais de peaux Remo ! Le son est typique des batteries Yamaha : puissant, chaud, dynamique et précis !
                    </p>
                </div>
            </div>
            <div className={"itemContainer"} onMouseEnter={() => {
                        stateItem5.selected = true;
                    }}
                    onMouseLeave={() => {
                        stateItem5.selected = false;
                    }}
                    onClick={() => {
                        setIndex(4);
                        setIsInfo(true);
                    }} >
                <div className="instrumentImg">
                    <Scene className="instrumentImg" path={path5} stateItem={stateItem5} />
                </div>
                <div className="instrumentText">
                    <h2 className="title">Piano</h2>
                    <p>
                    Ce piano à queue est conçu pour les espaces réduits, cet instrument au rapport qualité/prix absolument exceptionnel n'a pas d'égal dans sa catégorie.
                    Aujourd’hui, grâce à des améliorations substantielles des matériaux et dans les moyens de production, cet instrument expressif et abordable est devenu meilleur que jamais.
                    </p>
                </div>
            </div>
            <div className={"itemContainer"} onMouseEnter={() => {
                        stateItem6.selected = true;
                    }}
                    onMouseLeave={() => {
                        stateItem6.selected = false;
                    }}
                    onClick={() => {
                        setIndex(5);
                        setIsInfo(true);
                    }} >
                <div className="instrumentImg">
                    <Scene className="instrumentImg" path={path6} stateItem={stateItem6} />
                </div>
                <div className="instrumentText">
                    <h2 className="title">Balafon</h2>
                    <p>
                    Authentique instrument africain, le balafon FUZEAU 8 LAMES st entièrement fait à la main et tout en bois.
                    Réalisé avec des calebasses de tailles progressives (pour former les caisses de résonance), des lames de bois avec armatures en bambou et des cordons en peau de biche ou de chèvre (peaux les plus résistantes).
                    </p>
                </div>
            </div>
            <div className={"itemContainer"} onMouseEnter={() => {
                        stateItem7.selected = true;
                    }}
                    onMouseLeave={() => {
                        stateItem7.selected = false;
                    }}
                    onClick={() => {
                        setIndex(6);
                        setIsInfo(true);
                    }} >
                <div className="instrumentImg">
                    <Scene className="instrumentImg" path={path7} stateItem={stateItem7} />
                </div>
                <div className="instrumentText">
                    <h2 className="title">Basse electrique</h2>
                    <p>
                    La basse électrique est dotée d'un corps en Masonite léger, donnant à la basse son ton aérien caractéristique, elle possède un manche en Érable, une touche en palissandre de 21 frettes ainsi que les commandes, Master, Tonalité et Inverseur 2 voies.
                    Elle est aussi équipée de micros rouge à lèvres double bobinage de 15 ans, qui fournissent un son clair avec beaucoup de présence, de clarté et de twang haut de gamme.
                    </p>
                </div>
            </div>
            <div className={"itemContainer"} onMouseEnter={() => {
                        stateItem8.selected = true;
                    }}
                    onMouseLeave={() => {
                        stateItem8.selected = false;
                    }}
                    onClick={() => {
                        setIndex(7);
                        setIsInfo(true);
                    }} >
                <div className="instrumentImg">
                    <Scene className="instrumentImg" path={path8} stateItem={stateItem8} />
                </div>
                <div className="instrumentText">
                    <h2 className="title">Sanza ou Kalimba</h2>
                    <p>
                        Déclinable en plusieurs modes et gammes cette caisse en bois (ou en métal) souvent appelée kalimba pourvue de lamelles métalliques est l'instrument de voyage par excellence. Elle se tient au creux des deux mains et se joue avec les extrémités des deux pouces. Certains modèles peuvent aussi se jouer posés.
                    </p>
                </div>
            </div>
            <div className={"itemContainer"} onMouseEnter={() => {
                        stateItem9.selected = true;
                    }}
                    onMouseLeave={() => {
                        stateItem9.selected = false;
                    }}
                    onClick={() => {
                        setIndex(8);
                        setIsInfo(true);
                    }} >
                <div className="instrumentImg">
                    <Scene className="instrumentImg" path={path9} stateItem={stateItem9} />
                </div>
                <div className="instrumentText">
                    <h2 className="title">Viole de gambe</h2>
                    <p>
                    Cet instrument s’inspire d’une peinture du Dominiquin du 16e siècle, représentant sainte Cécile, patronne des musiciens. Le décor évoque la Renaissance italienne : ouïes en forme de motif de queue de poisson, armoiries et cartouches, mascarons, rinceaux de végétaux peints.
                    Tout noble se devait d’avoir une éducation musicale soignée. Contrairement au violon, instrument populaire réservé à l’accompagnement des danses et joué par des musiciens de rue, la viole de gambe était considérée comme un instrument noble.
                    </p>
                </div>
            </div>
            <div className={"itemContainer"} onMouseEnter={() => {
                        stateItem10.selected = true;
                    }}
                    onMouseLeave={() => {
                        stateItem10.selected = false;
                    }}
                    onClick={() => {
                        setIndex(9);
                        setIsInfo(true);
                    }} >
                <div className="instrumentImg">
                    <Scene className="instrumentImg" path={path10} stateItem={stateItem10} />
                </div>
                <div className="instrumentText">
                    <h2 className="title">Flûte traversière</h2>
                    <p>
                    Cette flute est caractérisé par une fabrication solide, elles est robuste et durable. La flûte traversière possède un mécanisme à un seul axe commun sans goupille qui apporte une excellente fluidité. Sa tête et son corps sont en maillechort argenté, la plaque et le noyau sont en argent massif.
                    Cette flûte bien connue des professeurs procure un timbre riche et une facilité de jeu idéale pour l'apprentissage.
                    </p>
                </div>
            </div>
        </div>
    )
}