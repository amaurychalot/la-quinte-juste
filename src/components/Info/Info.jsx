import SceneInfo from "../SceneInfo/SceneInfo";
import "./Info.scss";

export default function Info({ index, setIndex, setIsInfo }) {

    const descriptions = [
        "Ce magnifique violon massif 4/4 pourra vous accompagner dans toute votre splendeur. Etudié pour une grande durée de vie avec des matériaux choisis avec soin, ce violon a été conçu de la musique de chambre à la musique d'orchestrale.",
        "Un bec conçu pour révéler le meilleur de votre son. Ce saxophone est un parfail mélange entre facilité de jeu et justesse des notes. Il s'agit du parfait instrument pour briller sur scène.",
        "Cette guitare acoustique folk est disponible dès maintenant, idéale pour débuter la guitare, elle a une très bonne prise en main, avec un poids de 2kg, vous pourrez la transporter partout !",
        "Avec cette batterie votre jeu et votre créativité ne seront pas seulement parfaitement reproduits, mais également mis en valeur. Fûts fabriqués mains à partir de tilleul, équipée désormais de peaux Remo ! Le son est typique des batteries Yamaha : puissant, chaud, dynamique et précis !",
        "Ce piano à queue est conçu pour les espaces réduits, cet instrument au rapport qualité/prix absolument exceptionnel n'a pas d'égal dans sa catégorie. Aujourd’hui, grâce à des améliorations substantielles des matériaux et dans les moyens de production, cet instrument expressif et abordable est devenu meilleur que jamais.",
        "Authentique instrument africain, le balafon FUZEAU 8 LAMES st entièrement fait à la main et tout en bois. Réalisé avec des calebasses de tailles progressives (pour former les caisses de résonance), des lames de bois avec armatures en bambou et des cordons en peau de biche ou de chèvre (peaux les plus résistantes).",
        "La basse électrique est dotée d'un corps en Masonite léger, donnant à la basse son ton aérien caractéristique, elle possède un manche en Érable, une touche en palissandre de 21 frettes ainsi que les commandes, Master, Tonalité et Inverseur 2 voies. Elle est aussi équipée de micros rouge à lèvres double bobinage de 15 ans, qui fournissent un son clair avec beaucoup de présence, de clarté et de twang haut de gamme.",
        "Déclinable en plusieurs modes et gammes cette caisse en bois (ou en métal) souvent appelée kalimba pourvue de lamelles métalliques est l'instrument de voyage par excellence. Elle se tient au creux des deux mains et se joue avec les extrémités des deux pouces. Certains modèles peuvent aussi se jouer posés.",
        "Cet instrument s’inspire d’une peinture du Dominiquin du 16e siècle, représentant sainte Cécile, patronne des musiciens. Le décor évoque la Renaissance italienne : ouïes en forme de motif de queue de poisson, armoiries et cartouches, mascarons, rinceaux de végétaux peints. Tout noble se devait d’avoir une éducation musicale soignée. Contrairement au violon, instrument populaire réservé à l’accompagnement des danses et joué par des musiciens de rue, la viole de gambe était considérée comme un instrument noble.",
        "Cette flute est caractérisé par une fabrication solide, elles est robuste et durable. La flûte traversière possède un mécanisme à un seul axe commun sans goupille qui apporte une excellente fluidité. Sa tête et son corps sont en maillechort argenté, la plaque et le noyau sont en argent massif. Cette flûte bien connue des professeurs procure un timbre riche et une facilité de jeu idéale pour l'apprentissage."
    ]

    const titles = [
        "Violon 4/4 Massif",
        "Saxophone",
        "Guitare Acoustique Folk",
        "Batterie Kit Complet",
        "Piano à queue",
        "Fuzeau à 8 lames",
        "Basse électrique",
        "Kalimba",
        "Viole de gambe",
        "flûte traversière"
    ]

    /*const saledetails = [
        "<b>Disponibilités</b>",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
    ]*/

    const price = [
        "369,99€",
        "248,99€",
        "99,99€",
        "399,99€",
        "11 999,00€",
        "69,99€",
        "319,99€",
        "39,99€",
        "1699,99€",
        "1499,99€"
    ]

    /*const handleBackButton = function () {
        setIndex(-1);
        setIsInfo(false)
    }*/

    const handleLeftClick = function () {
        if (index === 0) {
            setIndex(9);
        } else {
            setIndex(index - 1);
        }
    }

    const handleRightClick = function () {
        if (index === 9) {
            setIndex(0);
        } else {
            setIndex(index + 1);
        }
    }

    return (
        <div className="infoContainer">
            {/*<button onClick={() => {handleBackButton()}}>Retour</button>
            Info sur l'objet {index}*/}
            <div className="itemContainer">
                <div className="topContainer">
                    <div className="viewContainer">
                        <div className="leftArrow" onClick={() => {handleLeftClick()}} />
                        <div className="canvaContainer">
                            <SceneInfo index={index} />
                        </div>
                        <div className="rightArrow" onClick={() => {handleRightClick()}} />
                    </div>
                    <div className="rightWindow">
                        <div className="container">
                            <h2>{price[index]}</h2>
                            <p>
                            <b>DISPONIBILITE</b>
                            <br/><span className="greenPoint">●</span><span className="alignText">Stock Internet: <b>2 Restants !</b></span><br/>Commandez avant 12h et recevez le demain!
                            <br/><span className="greenPoint">●</span><span className="alignText">Stock Magasin: <b>1 sur place!</b> </span>
                            <br/>
                            <br/><b>LIVRAISON</b>
                            <br/>Livraison: 16,99€
                            <br/>
                            <br/>
                            <b>GARANTIE</b>
                            <br/>Tous nos produits sont garantie <b>3 ans</b>!
                            </p>
                            <a><button className="buyButton bn27">Acheter</button></a>
                        </div>
                    </div>
                </div>
                <div className="bottomContainer">
                    <div className="textContainer">
                        <h2>{titles[index]}</h2>
                        <p>
                            {descriptions[index]}
                        </p>
                    </div>
                </div>
            </div>

        </div>
    )
}